package app.fedilab.android.exception;

public class DBException extends Exception {

    // Constructor that accepts a message
    public DBException(String message) {
        super(message);
    }
}
